## 代码拉下后
- 执行`composer dump-autoload` 初始化文件夹
- 运行示例子，部分需要config内容，不在版本库中。

## Composer 引入
- composer.json 文件增加如下配置

  ```
      "scripts":{
        "post-install-cmd":"Yjius\\EasyappYii\\components\\operateOperateAfter::postInstall",
        "post-update-cmd":"Yjius\\EasyappYii\\components\\operateOperateAfter::postUpdate"
      },
      "repositories": {
            "0": {
              "type": "composer",
              "url": "https://asset-packagist.org"
             },
            "1": {
              "type": "composer",
              "url": "https://packagist.org"
            }
       },
       "config": {
           "secure-http": false
         }
  ```

- 执行命令

  ``` composer require yjius/easyapp-yii --ignore-platform-reqs ```
  
  ```#忽略版本差异可加--ignore-platform-reqs ```

- 在main.php 文件中增加如下配置
 ```phpregexp
    'modules' => [
          'ucenter' => [
              'class' => 'Yjius\EasyappYii\modules\ucenter\Module',
          ]
        ]

```


  

