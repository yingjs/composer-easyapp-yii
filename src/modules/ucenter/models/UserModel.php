<?php

namespace Yjius\EasyappYii\modules\ucenter\models;

use Yjius\EasyappYii\components\traits\YiiModelQueryTrait;

class UserModel extends \yii\db\ActiveRecord
{
    use  YiiModelQueryTrait;

    public static function tableName()
    {
        return 'ea_user';
    }
}
