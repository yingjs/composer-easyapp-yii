<?php

namespace Yjius\EasyappYii\modules\ucenter\services;

use Yjius\EasyappYii\components\abstracts\ResponseHandler;

class ResponseService extends ResponseHandler
{
    public static $instance = null;

    //单例模式
    static public function getInstance()
    {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function error($msg = "fail", $data = [], $code = 500)
    {
        return ['msg' => $msg, 'data' => $data, 'code' => $code];
    }

    public function success($data = [], $msg = "success", $code = 200)
    {
        return ['msg' => $msg, 'data' => $data, 'code' => $code];
    }

}