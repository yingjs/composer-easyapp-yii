<?php

namespace Yjius\EasyappYii\modules\ucenter\services;


use Yjius\common\ToolsHelper;
use Yjius\EasyappYii\components\jwt\FirebaseJwt;
use Yjius\EasyappYii\modules\ucenter\models\TokenModel;

class UserService
{
    public static function generatePassword($password, $salt)
    {
        // 密码加密规则：md5(md5(原始密码) + 盐值)
        return md5(md5($password) . $salt);
    }

    public static function generateSalt()
    {
        return ToolsHelper::random(6);
    }

    public static function generateToken($userData)
    {
        $jwtData = FirebaseJwt::encode([
            "id" => $userData['id'],
            "username" => $userData['username'],
            "mobile" => $userData['mobile'],
            "create_date" => $userData['create_date']
        ]);

        TokenModel::saveDataNoExistAdd(
            [
                "user_id" => $userData['id'],
                "token" => $jwtData['jwt'],
                "expire_date" => date("Y-m-d H:i:s", $jwtData['payload']['exp']),
                "update_date" => date("Y-m-d H:i:s", $jwtData['payload']['nbf'])
            ], ['user_id' => $userData['id']]
        );
        return $jwtData['jwt'];
    }

}