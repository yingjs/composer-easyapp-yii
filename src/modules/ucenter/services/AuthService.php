<?php

namespace Yjius\EasyappYii\modules\ucenter\services;


use Yjius\EasyappYii\components\jwt\FirebaseJwt;

class AuthService
{

    public static function checkLogin($jwt)
    {
        //解析token
        //cando 验证表中token \Yii::$app->easyappYiiUcenter->ucenterConfig

        return FirebaseJwt::decode($jwt);
    }
}