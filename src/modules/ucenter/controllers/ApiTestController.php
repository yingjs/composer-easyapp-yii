<?php

namespace Yjius\EasyappYii\modules\ucenter\controllers;


use Yjius\common\Debug;

class ApiTestController extends UcenterController
{
    protected $noNeedLoginRoute = ['ucenter/api-test/not-token'];

    public function actionInfo()
    {
        $userData = $this->getLoginUser();
        $this->success($userData);
    }

    public function actionUserid()
    {
        $this->success($this->getLoginUser()['id']);
    }


    public function actionNotToken()
    {
        $this->success(["无需登录"]);
    }

}