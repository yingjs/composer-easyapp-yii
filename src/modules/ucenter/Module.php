<?php

namespace Yjius\EasyappYii\modules\ucenter;


class Module extends \yii\base\Module
{
    public $controllerNamespace = 'Yjius\EasyappYii\modules\ucenter\controllers';

    public function init()
    {
        parent::init();
    }
}