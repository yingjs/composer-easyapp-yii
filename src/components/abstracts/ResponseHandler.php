<?php

namespace Yjius\EasyappYii\components\abstracts;


abstract class ResponseHandler
{
    //错误返回
    abstract protected function error($msg = "fail", $data = [], $code = 500);

    //成功返回
    abstract protected function success($data = [], $msg = "success", $code = 200);

}