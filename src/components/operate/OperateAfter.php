<?php

namespace Yjius\EasyappYii\components\operate;
/**
 * composer安装后的类
 * Class OperateAfter
 * @package Yjius\libraries
 * @date  2022/6/15 11:25
 */
final class OperateAfter
{
    public static function postUpdate()
    {
        self::del();
    }

    public static function postInstall()
    {
        self::del();
    }

    private static function findGitDir($dir)
    {
        static $gitDirs = [];
        $items = scandir($dir);
        foreach ($items as $item) {
            if ($item != '.' && $item != '..') {
                if (!is_dir($dir . $item)) {
                    continue;
                } else {
                    if ($item == '.git') {
                        $gitDirs[] = $dir . $item;
                    } else {
                        self::findGitDir($dir . $item . DS);
                    }
                }
            }
        }
        return $gitDirs;
    }

    private static function del()
    {
        defined('DS') ?: define('DS', DIRECTORY_SEPARATOR);
        $baseDir = __DIR__ . DS . '..' . DS . '..' . DS . '..' . DS;
        //删除gitignore
        if (file_exists($baseDir . '.gitignore')) {
            @unlink($baseDir . '.gitignore');
        }
        $gitDirs = self::findGitDir($baseDir);
        if ($gitDirs) {
            if (strpos(strtoupper(PHP_OS), 'WIN') !== false) {
                foreach ($gitDirs as $gitDir) {
                    if (is_dir($gitDir)) {
                        @exec("rmdir /s /q  " . $gitDir);
                    }
                }
            } else {
                foreach ($gitDirs as $gitDir) {
                    if (is_dir($gitDir)) {
                        @exec("rm -rf " . $gitDir);
                    }
                }
            }
        }
        return true;
    }
}